import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  // { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'add-to-receipt', loadChildren: './add-to-receipt/add-to-receipt.module#AddToReceiptPageModule' },
  // { path: 'create-receipt', loadChildren: './add-to-receipt/create-receipt/create-receipt.module#CreateReceiptPageModule' },
  { path: 'success-receipt', loadChildren: './add-to-receipt/success-receipt/success-receipt.module#SuccessReceiptPageModule' },
  { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
  { path: 'history-detail', loadChildren: './history/history-detail/history-detail.module#HistoryDetailPageModule' },
  { path: 'setting-item', loadChildren: './add-to-receipt/setting-item/setting-item.module#SettingItemPageModule' },
  // { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
  // { path: 'history-detail', loadChildren: './history/history-detail/history-detail.module#HistoryDetailPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
