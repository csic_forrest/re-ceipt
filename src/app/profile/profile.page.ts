import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  name = 'Angular 4';
  url = '';
  reader;
  restoName: string;
  tax: number;
  service: number;

  constructor(private alertController: AlertController) { }

  ngOnInit() {
    this.restoName = "Merchantku";
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.reader = new FileReader();

      this.reader.readAsDataURL(event.target.files[0]);

      this.reader.onload = (event: { target: { result: string; }; }) => {
        this.url = event.target.result;
      }
    }
  }
  public delete(){
    this.url = null;
  }
  async editTax() {
    const alert = await this.alertController.create({
      header: 'Edit Tax',
      inputs: [
        {
          name: 'tax',
          type: 'number',
          id: 'tax-id',
          placeholder: 'Ganti tax...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: (data) => {
            this.changeTax(data.tax);
          }
        }
      ]
    });

    await alert.present();
  }

  async editService() {
    const alert = await this.alertController.create({
      header: 'Edit Service',
      inputs: [
        {
          name: 'service',
          type: 'number',
          id: 'service-id',
          placeholder: 'Ganti service...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: (data) => {
            this.changeService(data.service);
          }
        }
      ]
    });

    await alert.present();
  }

  async editRestaurantName() {
    const alert = await this.alertController.create({
      header: 'Edit Restaurant Name',
      inputs: [
        {
          name: 'restaurantname',
          type: 'text',
          id: 'rname-id',
          value: '',
          placeholder: 'Ganti nama...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: (data) => {
            this.changeName(data.restaurantname);
          }
        }
      ]
    });

    await alert.present();
  }


  public changeName(restoku){
    console.log('test123');
    this.restoName = restoku
  } 

  public changeTax(tax){
    this.tax = tax;
  }

  public changeService(service){
    this.service = service;
  }
}
