import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: HomePage,
        children: [
            {
                path:  'beranda',
                children: [
                    {
                        path: '',
                        loadChildren: './beranda/beranda.module#BerandaPageModule'
                    },
                    {
                        path: 'show-qr-code', 
                        loadChildren: './beranda/show-qr-code/show-qr-code.module#ShowQrCodePageModule' 
                    }
                ]
            },
            {
                path: 'add-to-receipt',
                children: [
                    {
                        path: '',
                        loadChildren: '../add-to-receipt/add-to-receipt.module#AddToReceiptPageModule'
                    },
                    { 
                        path: 'create',
                        loadChildren: '../add-to-receipt/create-receipt/create-receipt.module#CreateReceiptPageModule' 
                    }
                ]
            },
            {
                path: 'history',
                children: [
                    {
                        path: '',
                        loadChildren: '../history/history.module#HistoryPageModule'
                    }
                ]
            },
            {
                path: 'profile',
                children: [
                    {
                        path: '',
                        loadChildren: '../profile/profile.module#ProfilePageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/home/tabs/beranda',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/home/tabs/beranda',
        pathMatch: 'full'
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})


export class HomeRoutingModule {}
