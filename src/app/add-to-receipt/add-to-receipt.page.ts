import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-to-receipt',
  templateUrl: './add-to-receipt.page.html',
  styleUrls: ['./add-to-receipt.page.scss'],
})
export class AddToReceiptPage implements OnInit {
  amount: number;
  constructor() { }

  ngOnInit() {
    this.amount = 0;
    console.log('asd')
  }


  public incrementAmount(){
    this.amount += 1;
  }

  public decrementAmount(){
    this.amount -= 1;
    if(this.amount < 0 ) this.amount = 0;
  }

}
