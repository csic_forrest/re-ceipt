import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingItemPage } from './setting-item.page';

describe('SettingItemPage', () => {
  let component: SettingItemPage;
  let fixture: ComponentFixture<SettingItemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingItemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
