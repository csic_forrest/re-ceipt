import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-setting-item',
  templateUrl: './setting-item.page.html',
  styleUrls: ['./setting-item.page.scss'],
})
export class SettingItemPage implements OnInit {

  constructor(private alertController: AlertController) { }

  ngOnInit() {
  }

  async onEditData() {
    const alert = await this.alertController.create({
      header: 'Edit Food',
      inputs: [
        {
          name: 'food.id',
          type: 'text',
          id: 'tax-id',
          value: 'Nasi Goreng'
        },
        {
          name: 'food.price',
          type: 'number',
          id: 'tax-id',
          value: '20000'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }
  async onDeleteData() {
    const alert = await this.alertController.create({
      header: 'Delete Food',
      message: 'Delete this?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

}
