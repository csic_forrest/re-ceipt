import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessReceiptPage } from './success-receipt.page';

describe('SuccessReceiptPage', () => {
  let component: SuccessReceiptPage;
  let fixture: ComponentFixture<SuccessReceiptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessReceiptPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessReceiptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
