import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToReceiptPage } from './add-to-receipt.page';

describe('AddToReceiptPage', () => {
  let component: AddToReceiptPage;
  let fixture: ComponentFixture<AddToReceiptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddToReceiptPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToReceiptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
